FROM node:8.12.0-jessie

EXPOSE 3000

WORKDIR /app

ADD package.json /app/

RUN npm install

ADD . /app/

CMD ["node", "index"]
